# Developer Guide

## Pushing a new version of the gem

**Note:** You must have been granted access to deploy new versions of the `fuck_facebook` gem

* Bump the version in `lib/fuck_facebook/version.rb`
* Build the gem with `gem build fuck_facebook.gemspec`
* Push the gem with `gem push fuck_facebook-VERSION.gem` replacing `VERSION` with the new version number

## Pushing a new version of the Docker image

**Note:** You must have been granted access to deploy new versions of the `fuck_facebook` Docker image

* Build the image:

```
docker build . -t keeyan/fuck_facebook:VERSION --build-arg version=VERSION
```

Replace `VERSION` with the version number

* Push the image with `docker push keeyan/fuck_facebook:VERSION` replacing `VERSION` with the version number
