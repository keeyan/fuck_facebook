## [0.1.0] - 2021-07-15

- Send unread messages to your email address
- Initial release
