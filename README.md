# Fuck Facebook

A collection of tools to help you use Facebook when you don't want to

## Usage

Run `fuck-facebook --help` to get a list of options and explanations.

To get all unread messages (that haven't already been reported) run these commands:

```
fuck-facebook --login
```

You only need to login once

```
fuck-facebook --unread-messages
```

This will only show you unread messages that have been received since the last time you checked.
This way if you have emails going out for new messages, you won't get notified for the same message each time.

## Config

Here's a complete config file along with an explanation of each option:

```yaml
errors:
  min_minutes_between_reports: 0 # Only report error if one has not occured in the past X minutes
  include_screenshot: false # Generates a screenshot with the error report
smtp:
  from_email: sender@example.com
  to_email: receiver@example.com
  address: smtp.example.com
  port: 587
  user_name: some_user_name
  password: 123456
  authentication: plain
```

Place this file in `~/.local/share/fuck-facebook/config.yaml`

These options can be passed as environment variables, by:
* Joining the keys with `_`
* Capitalizing the entire string
* Prefixing the key with `FF_`

For example the `smtp` `from_email` will become `FF_SMTP_FROM_EMAIL`

Environment variables take precedent over the config file.
