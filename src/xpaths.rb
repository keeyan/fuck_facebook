require 'yaml'

class Xpaths
  attr_reader :driver

  def initialize(driver)
    @driver = driver
  end

  def element(*path_elements)
    element_xpath = self.for(*path_elements)

    driver.find_element(:xpath, element_xpath)
  end

  def child_element(element, *path_elements)
    element.find_element(:xpath, self.for(*path_elements))
  end

  def element_exists?(*path_elements)
    element(*path_elements)
    true
  rescue Selenium::WebDriver::Error::NoSuchElementError
    false
  end

  def element_has_child?(element, *path_elements)
    child_element(element, *path_elements)
    true
  rescue Selenium::WebDriver::Error::NoSuchElementError
    false
  end

  def elements(*path_elements)
    element_xpath = self.for(*path_elements)

    driver.find_elements(:xpath, element_xpath)
  end

  def element_containing_text(text)
    results = elements_containing_text(text)

    raise "Too many elements found containing text '#{text}'" if results.count > 1
    raise "No elements found containing text '#{text}'" if results.count.zero?

    results[0]
  end

  def elements_containing_text(text)
    driver.find_elements(:xpath, "//*[text() = '#{text}']")
  end

  def wait_for_element(*path_elements)
    found_element = nil

    Selenium::WebDriver::Wait.new.until do
      found_element = element(*path_elements)
      true
    rescue Selenium::WebDriver::Error::TimeoutError
      false
    end

    found_element
  end

  def wait_for_elements(*path_elements)
    wait_for_element(*path_elements)

    elements(*path_elements)
  end

  def for(*path_elements)
    path_elements_strings = path_elements.map(&:to_s)
    result = XPATHS.dig(*path_elements_strings)

    raise 'Could not find xpath' if result.nil? || result.empty?

    result
  end

  private_class_method def self.xpaths_from_file
    xpaths_file = File.join(File.dirname(__FILE__), '../xpaths.yaml')
    YAML.load_file(xpaths_file)
  end

  XPATHS = xpaths_from_file
end
