require 'highline'

class UserInput
  attr_reader :highline, :saved_answers

  def initialize
    @highline = HighLine.new
    @saved_answers = {}
  end

  def get(value, text = nil, secret: false, remember: true)
    return saved_answers[value] if saved_answers.key?(value) && remember

    saved_answers[value] = highline.ask("#{text || value}: ") { _1.echo = !secret }
  end
end
