module Senders
  class Email
    def self.send_messages(messages)
      messages.each do |message|
        Mail.deliver do
          from Config.option(:smtp, :from_email)
          to Config.option(:smtp, :to_email)
          subject "New Facebook message on chat #{message.message_thread}"
          body message.text
        end
      end
    end

    def self.send_error(error, screenshot: nil)
      Mail.deliver do
        from Config.option(:smtp, :from_email)
        to Config.option(:smtp, :to_email)
        subject 'Error occurred during Fuck Facebook command'
        body error.full_message(highlight: false)
        add_file filename: 'error_screenshot.png', content: screenshot if screenshot
      end
    end
  end
end
