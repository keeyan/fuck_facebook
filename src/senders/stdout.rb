module Senders
  class Stdout
    def self.send_messages(messages)
      messages.each do |message|
        puts message
      end
    end

    def self.send_error(error, screenshot: nil)
      puts error
      return if screenshot.nil?

      filename = "fuck_facebook_error_#{Time.now.iso8601}.png"
      File.write(filename, screenshot)
      puts "Screenshot of error page saved as #{filename}"
    end
  end
end
