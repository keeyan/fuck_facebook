class MessageSender
  attr_accessor :sender_name, :facebook_connection

  def initialize(sender_name, facebook_connection)
    @sender_name = sender_name
    @facebook_connection = facebook_connection
  end

  def send_unread_messages
    messages = facebook_connection.message_handler.unread
    send_array_of_messages(messages)
  end

  def send_messages
    messages = facebook_connection.message_handler.messages
    send_array_of_messages(messages)
  end

  private

  def send_array_of_messages(messages)
    sender = Senders.by_name(sender_name)
    sender.send_messages(messages)
  end
end
