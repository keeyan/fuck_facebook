class ArgsParser
  DEFAULT_OPTIONS = {
    headless: true,
    clear_cookies: false,
    unread_messages: false,
    messages: false,
    sender: 'stdout',
    simulate_error: false
  }.freeze

  AVAILABLE_OPTIONS = [
    {
      short: '-h',
      long: '--[no-]headless',
      message: 'Run in a headless browser',
      option: :headless
    },
    {
      short: nil,
      long: '--clear-cookies',
      message: 'Clears all cookies',
      option: :clear_cookies
    },
    {
      short: nil,
      long: '--unread-messages',
      message: 'Outputs all unread messages sent since the last check',
      option: :unread_messages
    },
    {
      short: nil,
      long: '--messages',
      message: 'Outputs all messages sent since the last check',
      option: :messages
    },
    {
      short: nil,
      long: '--login',
      message: 'Prompts for details to login to Facebook account',
      option: :login
    },
    {
      short: nil,
      long: '--sender=SENDER',
      message: 'Send results to either stdout or email',
      option: :sender
    },
    {
      short: nil,
      long: '--simulate-error',
      message: 'Simulates an error so you can see how it will be outputted',
      option: :simulate_error
    }
  ].freeze

  def self.parse_args
    passed_options = DEFAULT_OPTIONS.dup

    OptionParser.new do |opts|
      generate_banner(opts)

      AVAILABLE_OPTIONS.each do |available_option|
        parser_for_available_option(opts, available_option, passed_options)
      end
    end.parse!

    passed_options
  end

  private_class_method def self.generate_banner(opts)
    opts.banner = 'Usage: fuck-facebook.rb [options]'
  end

  private_class_method def self.parser_for_available_option(opts, available_option, passed_options)
    args = [available_option[:long], available_option[:message]]
    args = [available_option[:short]] + args if available_option[:short]

    opts.on(*args) do |v|
      passed_options[available_option[:option]] = v
    end
  end
end
