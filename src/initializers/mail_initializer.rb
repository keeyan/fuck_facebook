require_relative '../config'

Mail.defaults do
  delivery_method :smtp, {
    address: Config.option(:smtp, :address),
    port: Config.option(:smtp, :port),
    user_name: Config.option(:smtp, :user_name),
    password: Config.option(:smtp, :password),
    authentication: Config.option(:smtp, :authentication),
    enable_starttls_auto: true
  }
end
