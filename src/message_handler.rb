require_relative 'fb_duration'
require_relative 'storage'
require_relative 'models/message'
require_relative 'models/user'
require_relative 'models/message_thread'

class MessageHandler
  attr_reader :driver

  def initialize(driver)
    @driver = driver
  end

  def unread
    messages.reject(&:read?)
  end

  def messages
    visit_messages_page

    chats = xpaths.wait_for_elements(:messages, :chats)
    messages = messages_for_chats(chats)

    messages_after_last_message_time(messages)
  end

  private

  def messages_for_chats(chats)
    chats.map do |chat|
      message_thread = MessageThread.new(title: xpaths.child_element(chat, :messages, :title_relative).text)

      Message.new(
        message_thread: message_thread,
        text: chat_text(chat),
        read: chat_read?(chat),
        timestamp: chat_timestamp(chat)
      )
    end
  end

  def chat_text(chat)
    xpaths.child_element(chat, :messages, :text_relative).text
  end

  def chat_read?(chat)
    !xpaths.element_has_child?(chat, :messages, :mark_as_read_button_relative)
  end

  def chat_timestamp(chat)
    message_time_ago = xpaths.child_element(chat, :messages, :time_sent_ago_relative).text
    FbDuration.parse_to_time(message_time_ago)
  end

  def messages_after_last_message_time(messages)
    last_message_time = Storage.get(:last_message_time, default: Time.new(2004, 2, 4))

    messages_to_return = messages.select { _1.timestamp > last_message_time }

    last_message_time = messages.map(&:timestamp).max

    Storage.set(:last_message_time, last_message_time)

    messages_to_return
  end

  def xpaths
    @xpaths ||= Xpaths.new(driver)
  end

  def visit_messages_page
    driver.navigate.to('https://www.facebook.com/messages/')
  end
end
