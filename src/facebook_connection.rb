require 'selenium-webdriver'

require_relative './authenticator'
require_relative './message_handler'
require_relative './cookie_handler'

class FacebookConnection
  attr_reader :headless

  def initialize(headless:)
    @headless = headless

    cookie_handler.load
  end

  def login
    authenticator = Authenticator.new(driver)
    authenticator.login
  end

  def close
    cookie_handler.save
  end

  def message_handler
    @message_handler ||= MessageHandler.new(driver)
  end

  def driver
    return @driver if defined? @driver

    options = Selenium::WebDriver::Chrome::Options.new
    options.add_argument('--disable-notifications')
    options.add_argument('--no-sandbox')
    options.add_argument('--disable-dev-shm-usage')
    options.add_argument('--headless') if headless
    options.add_option('detach', true) unless headless

    @driver = Selenium::WebDriver.for :chrome, options: options

    @driver.manage.window.resize_to(1920, 1080)

    @driver
  end

  def cookie_handler
    @cookie_handler ||= CookieHandler.new(driver)
  end
end
