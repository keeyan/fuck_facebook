class Message
  attr_accessor :message_thread, :text, :read, :timestamp

  def initialize(message_thread:, text:, timestamp:, read: true)
    @message_thread = message_thread
    @text = text
    @read = read
    @timestamp = timestamp
  end

  def read?
    read
  end

  def to_s
    "#{message_thread}: #{text}"
  end
end
