require 'fileutils'
require 'time'

class CookieHandler
  attr_reader :driver

  def initialize(driver)
    @driver = driver
  end

  def clear!
    File.delete(cookies_filename) if File.exist? cookies_filename

    driver.manage.delete_all_cookies
  end

  def save
    FileUtils.mkdir_p(cookies_dir)
    File.write(cookies_filename, current_cookies.to_json)
  end

  def load
    current_domain = nil
    saved_cookies.each do |saved_cookie|
      new_domain = remove_leading_period(saved_cookie[:domain])

      driver.navigate.to "https://#{new_domain}" unless new_domain == current_domain
      current_domain = new_domain

      driver.manage.add_cookie(saved_cookie)
    end
  end

  def current_cookies
    driver.manage.all_cookies
  end

  def saved_cookies
    return {} unless File.exist? cookies_filename

    saved_cookies = JSON.parse(File.read(cookies_filename), symbolize_names: true)

    saved_cookies.each do |saved_cookie|
      saved_cookie[:expires] = Time.parse(saved_cookie[:expires]) unless saved_cookie[:expires].nil?
    end

    saved_cookies
  end

  def cookies_filename
    @cookies_filename ||= File.join(cookies_dir, 'cookies.json')
  end

  def cookies_dir
    @cookies_dir ||= File.join(ENV['HOME'], '.local/share/fuck-facebook/')
  end

  private

  def remove_leading_period(str)
    str[1..]
  end
end
