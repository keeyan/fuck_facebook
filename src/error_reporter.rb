class ErrorReporter
  def self.report_error(sender_name, error, facebook_connection: nil)
    raise error unless report_error?

    screenshot = include_screenshot?(facebook_connection) ? create_screenshot(facebook_connection) : nil

    Senders.by_name(sender_name).send_error(error, screenshot: screenshot)

    Storage.set(:last_error_reported_time, Time.now)
    raise error
  end

  private_class_method def self.include_screenshot?(facebook_connection)
    include_screenshot_config = Config.option(
      :errors,
      :include_screenshot,
      default: false,
      type: :boolean
    )

    include_screenshot_config && facebook_connection.present?
  end

  private_class_method def self.create_screenshot(facebook_connection)
    facebook_connection.driver.screenshot_as(:png)
  rescue StandardError
    nil
  end

  private_class_method def self.report_error?
    min_minutes_between_reports = Config.option(
      :errors,
      :min_minutes_between_reports,
      default: 0,
      type: :float
    )

    last_error_reported_time = Storage.get(:last_error_reported_time, default: Time.new(2000))

    next_report_time = last_error_reported_time + min_minutes_between_reports.minutes

    Time.now > next_report_time
  end
end
