require_relative 'senders/email'
require_relative 'senders/stdout'

module Senders
  SENDERS = {
    stdout: Stdout,
    email: Email
  }.freeze

  def self.by_name(name)
    SENDERS[name.to_sym]
  end
end
