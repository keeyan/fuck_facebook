require 'active_support/core_ext/numeric/time'

class FbDuration
  DURATION_LETTER_TO_METHOD = {
    'm' => :minutes,
    'h' => :hours,
    'd' => :days,
    'w' => :weeks,
    'y' => :years
  }.freeze

  DURATION_LETTER_TO_BEGINNING_OF_METHOD = {
    'm' => :beginning_of_minute,
    'h' => :beginning_of_hour,
    'd' => :beginning_of_day,
    'w' => :beginning_of_week,
    'y' => :beginning_of_year
  }.freeze

  # TODO: Fill in d w y extra time to add
  ROUNDING_CORRECTION_TIMES = {
    'm' => 0,
    'h' => -5.minutes,
    'd' => 0,
    'w' => 0,
    'y' => 0
  }

  def self.parse_to_time(facebook_format_duration)
    duration = parse(facebook_format_duration)
    time_to_beginning_of_duration_letter(Time.now - duration, facebook_format_duration)
  end

  def self.parse(facebook_format_duration)
    number, duration_letter = facebook_format_duration.split

    rounding_correction_times = ROUNDING_CORRECTION_TIMES[duration_letter]

    number.to_i.send(DURATION_LETTER_TO_METHOD[duration_letter]) + rounding_correction_times
  end

  def self.time_to_beginning_of_duration_letter(time, facebook_format_duration)
    duration_letter = facebook_format_duration.split[1]

    time.send(DURATION_LETTER_TO_BEGINNING_OF_METHOD[duration_letter])
  end
end
