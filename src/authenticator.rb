require 'selenium-webdriver'

require_relative 'xpaths'
require_relative 'config'
require_relative 'user_input'

class Authenticator
  attr_reader :driver, :xpaths, :user_input

  def initialize(driver)
    @driver = driver
    @xpaths = Xpaths.new(driver)
    @user_input = UserInput.new
  end

  def login
    driver.navigate.to 'https://www.facebook.com/'
    return if logged_in?

    accept_cookies
    submit_credentials
    submit_otp
    click_remember_password
  end

  def logged_in?
    driver.navigate.to 'https://www.facebook.com/'
    xpaths.element_exists?(:link_to_profile)
  end

  private

  def click_remember_password
    driver.navigate.to('https://www.facebook.com/settings?tab=security')
    driver.switch_to.frame(xpaths.wait_for_element(:settings, :security_iframe))
    xpaths.element(:settings, :save_login_information_edit_button).click
    xpaths.element(:settings, :save_login_information_button).click
  end

  def submit_credentials
    xpaths.element(:email_address_input).send_keys(user_input.get(:email_address))
    xpaths.element(:password_input).send_keys(user_input.get(:password, secret: true))
    xpaths.element(:login_button).click
  end

  def submit_otp
    return unless xpaths.element_exists?(:checkpoint, :otp_prompt)

    xpaths.element(:checkpoint, :otp_input).send_keys(user_input.get(:otp))
    otp_button = xpaths.element(:checkpoint, :submit_button)
    otp_button.click

    save_browser_button = xpaths.element(:checkpoint, :submit_button)
    save_browser_button.click
  end

  def accept_cookies
    return unless xpaths.element_exists?(:accept_cookies_button)

    accept_cookies_button = xpaths.element(:accept_cookies_button)
    accept_cookies_button.click
  end
end
