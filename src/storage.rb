require 'fileutils'
require 'yaml'

class Storage
  STORAGE_FILE_PATH = "#{ENV['HOME']}/.local/share/fuck-facebook/storage.yaml".freeze

  def self.get(*keys, default: nil)
    value = storage.dig(*keys)

    return value if value

    default
  end

  def self.set(*keys, value)
    create_storage_file_if_not_exists!

    new_storage = storage

    current_level_hash = new_storage
    keys.each_with_index do |key, index|
      current_level_hash[key] = index + 1 == keys.count ? value : current_level_hash.fetch(key, {})
      current_level_hash = current_level_hash[key]
    end

    File.write(STORAGE_FILE_PATH, new_storage.to_yaml)
  end

  def self.storage
    create_storage_file_if_not_exists!

    YAML.load_file(STORAGE_FILE_PATH)
  end

  def self.create_storage_file_if_not_exists!
    dirname = File.dirname(STORAGE_FILE_PATH)
    FileUtils.mkdir_p(dirname) unless File.directory?(dirname)
    File.write(STORAGE_FILE_PATH, '{}') unless File.exist?(STORAGE_FILE_PATH)
  end
end
