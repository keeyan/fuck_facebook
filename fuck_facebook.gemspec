require_relative 'lib/fuck_facebook/version'

Gem::Specification.new do |spec|
  spec.name = 'fuck_facebook'
  spec.version = FuckFacebook::VERSION
  spec.authors = ['Keeyan Nejad']
  spec.email = ['keeyan@keeyan.xyz']

  spec.summary = 'A collection of tools to help you use Facebook when you don\'t want to'
  spec.description = <<~DESCRIPTION
    For now you can just set up a cronjob to email you about new messages,
    but more features will come as I find more things that annoy me about Facebook
  DESCRIPTION
  spec.homepage = 'https://gitlab.com/keeyan/fuck_facebook'
  spec.license = 'MIT'
  spec.required_ruby_version = Gem::Requirement.new('>= 2.7.0', '< 3.1')

  spec.metadata['allowed_push_host'] = 'https://rubygems.org'

  spec.metadata['homepage_uri'] = spec.homepage
  spec.metadata['source_code_uri'] = 'https://gitlab.com/keeyan/fuck_facebook'
  spec.metadata['changelog_uri'] = 'https://gitlab.com/keeyan/fuck_facebook/-/blob/main/CHANGELOG.md'

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{\A(?:test|spec|features)/}) }
  end
  spec.executables   = %w[fuck-facebook]
  spec.require_paths = ['lib']

  spec.add_dependency 'activesupport', '~> 6.1'
  spec.add_dependency 'highline', '~> 2.0'
  spec.add_dependency 'mail', '~> 2.7'
  spec.add_dependency 'selenium-webdriver', '~> 3.142'
end
